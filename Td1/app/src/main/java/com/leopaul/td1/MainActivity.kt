package com.leopaul.td1

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem

import kotlinx.android.synthetic.main.activity_main.*
import android.widget.TextView
import android.widget.EditText
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.view.View
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var etGauche: EditText
    private lateinit var etDroit: EditText

    private lateinit var addResult: TextView
    private lateinit var subResult: TextView
    private lateinit var multResult: TextView
    private lateinit var divResult: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        etGauche = editText
        etDroit = editText2

        addResult = textView
        subResult = textView2
        multResult = textView3
        divResult = textView4
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun performOperations(v: View) {
        val g = this.etGauche.text.toString()
        val d = this.etDroit.text.toString()

        this.computeAdd(g, d)
        this.computeSub(g, d)
        this.computeMult(g, d)
        this.computeDiv(g, d)
    }

    fun computeAdd(g: String, d: String) {
        this.addResult.text = if (g!="" && d!=""){
            (g.toInt() + d.toInt()).toString()
        } else {
            ""
        }
    }

    fun computeSub(g: String, d: String) {
        this.subResult.text = if (g!="" && d!=""){
            (g.toInt() - d.toInt()).toString()
        } else {
            ""
        }
    }

    fun computeMult(g: String, d: String) {
        this.multResult.text = if (g!="" && d!=""){
            (g.toInt() * d.toInt()).toString()
        } else {
            ""
        }
    }

    fun computeDiv(g: String, d: String) {
        this.divResult.text = if (g!="" && d!="" && d.toInt()!=0){
            (g.toInt() / d.toInt()).toString()
        } else {
            ""
        }
    }
}
