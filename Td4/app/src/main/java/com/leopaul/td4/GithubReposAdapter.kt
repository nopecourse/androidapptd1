package com.leopaul.td4

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class GithubReposAdapter(private val listRepos: List<Repo>) : RecyclerView.Adapter<GithubReposViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GithubReposViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_github_repo, parent, false)
        return GithubReposViewHolder(view)
    }

    override fun onBindViewHolder(holder: GithubReposViewHolder, position: Int) {
        holder.display(listRepos[position])
    }

    override fun getItemCount() = listRepos.size
}