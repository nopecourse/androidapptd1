package com.leopaul.td4

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_github_repo.view.*

class GithubReposViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val name: TextView = itemView.name

    fun display(repo: Repo) {
        name.text = repo.name
    }
}