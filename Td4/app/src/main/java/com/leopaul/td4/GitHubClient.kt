package com.leopaul.td4

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface GitHubClient {

    @GET("users/{username}/repos")
    fun UserRepositories(@Path("username") username: String): Call<List<Repo>>
}