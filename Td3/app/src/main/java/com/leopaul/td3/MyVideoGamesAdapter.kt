package com.leopaul.td3

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class MyVideoGamesAdapter(private val listJeux : List<JeuVideo>) : RecyclerView.Adapter<MyVideoGamesViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyVideoGamesViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_jeu_video, parent, false)
        return MyVideoGamesViewHolder(view)
    }

    override fun getItemCount() = listJeux.size

    override fun onBindViewHolder(holder: MyVideoGamesViewHolder, position: Int) {
        holder.display(listJeux[position])
    }
}