package com.leopaul.td3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var mesJeux = mutableListOf<JeuVideo>()
        mesJeux.add(JeuVideo("GOW", 666))
        mesJeux.add(JeuVideo( "Fable", 999))

        myRecyclerView.layoutManager = LinearLayoutManager(this)
        myRecyclerView.adapter = MyVideoGamesAdapter(mesJeux)
        myRecyclerView.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL,false)
    }

}
