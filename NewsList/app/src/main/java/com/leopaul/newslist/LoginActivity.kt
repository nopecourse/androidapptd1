package com.leopaul.newslist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    private val tag = "NewsList"

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.login_button -> {
                val intent = Intent(this, NewsActivity::class.java)
                startActivity(intent)
                this.finish()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        login_button.setOnClickListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(tag, "$localClassName activity terminated")
    }
}
