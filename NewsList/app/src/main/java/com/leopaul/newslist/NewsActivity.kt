package com.leopaul.newslist

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_news.*

class NewsActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.details_button -> {
                val intent = Intent(this, DetailsActivity::class.java)
                startActivity(intent)
            }
            R.id.logout_button -> {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                this.finish()
            }
            R.id.about_button -> {
                val url = "http://android.busin.fr/"
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(intent)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)

        details_button.setOnClickListener(this)
        logout_button.setOnClickListener(this)
    }
}
